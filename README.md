# Midi Heros et Dragons


Le partenaire idéal du module Héros et dragons.
Plusieurs compendiums pour des sorts, capacités et objets configurés pour être utilisé avec Midi qol, Dynamic Active Effect, times up et Item macro.  

##  Release 0.2

- Animation d'Inspiration bardique
### Ajout
- Représailles infernales
- Putréfaction
- Illusion mineure
- Blessure
- Sommeil
- Déguisement
- Instrument fantomatique
- Explosion occulte Amelioré
- Flamme sacrée
- Poison Naturel
- Soin des blessures taromancie
- Simulacre de vie
- Rayon de givre
- Bouffée de poison
- Aura du héros
- Briser
- Prière de soins
- Contact glacial
- Lames explosives
- Putréfaction
- Charme-personne
- Baies nourricières
- Soin des blessures
- Enchevêtrement
- Aura du héros
- Lame de feu
- Passage sans trace