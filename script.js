Hooks.once("init", function () {
    game.settings.register('midi-heros-et-dragons', 'Animation', {
        name: "Activer les animation",
        hint: "Permet d'activer les animations custom. Attention cela necessite l'activation de sequencer et le module patreon de JB2A",
        scope: "world",
        config: true,
        default: false,
        type: Boolean,
        onChange: () => window.location.reload()
    });
});